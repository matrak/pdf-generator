const request = require('request');

const base = ' https://lwbwbi3ja3.execute-api.ap-southeast-2.amazonaws.com/dev';
const fName = 'qr';

module.exports = {
  async generate(data) {
    const start = new Date();
    return new Promise((resolve, reject) => {
      request.post(`${base}/${fName}`, { json: { qrPayload: data } }, (err, res, body) => {
        console.log(`${new Date().getTime() - start.getTime()}ms`);
        if(err) reject(err);
        else resolve(body);
      });
    })

  }
}